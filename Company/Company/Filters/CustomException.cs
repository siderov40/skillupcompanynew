﻿using Company.DTOModels;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System.Net;

namespace Company.Filters
{
    public class CustomException : ExceptionFilterAttribute
    {
        public async override Task OnExceptionAsync(ExceptionContext context)
        {
            await base.OnExceptionAsync(context);

            var type = context.Exception.GetType();

            ExceptionHandling exceptionHandling = new ExceptionHandling();

            if (type == typeof(UnauthorizedAccessException))
            {
                exceptionHandling.message = "Unauthorized ";
                exceptionHandling.status = (int)HttpStatusCode.Unauthorized;
                exceptionHandling.hasError = true;
                exceptionHandling.innerException = context.Exception.InnerException.Message;
            }
            else if (type == typeof(Exception))
            {
                exceptionHandling.message = context.Exception.Message;
                exceptionHandling.status = (int)HttpStatusCode.InternalServerError;
                exceptionHandling.hasError = true;
                exceptionHandling.innerException = context.Exception.InnerException != null 
                    ?context.Exception.InnerException.Message : "Filter";
            }

            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)HttpStatusCode.InternalServerError; ;
            response.ContentType = "application/json";


            await response.WriteAsync(JsonConvert.SerializeObject(exceptionHandling));
        }
    }
}
