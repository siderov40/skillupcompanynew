﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Company.Migrations
{
    /// <inheritdoc />
    public partial class IdentityTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            //migrationBuilder.CreateTable(
            //    name: "Department",
            //    columns: table => new
            //    {
            //        DepartmentId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        DepartmentName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
            //        DepartmentNumber = table.Column<int>(type: "int", nullable: false),
            //        ManagerStartDate = table.Column<DateTime>(type: "datetime", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__Departme__B2079BEDD5972032", x => x.DepartmentId);
            //    });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            //migrationBuilder.CreateTable(
            //    name: "DepartmentLocations",
            //    columns: table => new
            //    {
            //        DepartmentLocationId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        DepartmentLocationName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
            //        DepartmentIdFK = table.Column<int>(type: "int", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__Departme__B6760B13BF8AADBB", x => x.DepartmentLocationId);
            //        table.ForeignKey(
            //            name: "FK__Departmen__Depar__398D8EEE",
            //            column: x => x.DepartmentIdFK,
            //            principalTable: "Department",
            //            principalColumn: "DepartmentId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Employee",
            //    columns: table => new
            //    {
            //        EmpId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Ssn = table.Column<string>(type: "varchar(14)", unicode: false, maxLength: 14, nullable: false),
            //        Gender = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
            //        EmpAddress = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
            //        EmpFirstName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
            //        EmpMiddleName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
            //        EmpLastName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
            //        Salary = table.Column<int>(type: "int", nullable: false),
            //        SupervisorId = table.Column<int>(type: "int", nullable: true),
            //        DepartmentId = table.Column<int>(type: "int", nullable: true),
            //        EmpDateOfBirth = table.Column<DateTime>(type: "datetime", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__Employee__AF2DBB99E15CC0EC", x => x.EmpId);
            //        table.ForeignKey(
            //            name: "FK__Employee__Depart__3D5E1FD2",
            //            column: x => x.DepartmentId,
            //            principalTable: "Department",
            //            principalColumn: "DepartmentId",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK__Employee__Superv__3C69FB99",
            //            column: x => x.SupervisorId,
            //            principalTable: "Employee",
            //            principalColumn: "EmpId");
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Project",
            //    columns: table => new
            //    {
            //        ProjectId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        ProjectName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
            //        ProjectNumber = table.Column<int>(type: "int", nullable: false),
            //        DepartmentIdFk = table.Column<int>(type: "int", nullable: false),
            //        ProjectLocation = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__Project__761ABEF014C29242", x => x.ProjectId);
            //        table.ForeignKey(
            //            name: "FK__Project__Departm__4316F928",
            //            column: x => x.DepartmentIdFk,
            //            principalTable: "Department",
            //            principalColumn: "DepartmentId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Dependent",
            //    columns: table => new
            //    {
            //        DependentId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        DependentName = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
            //        Gender = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: false),
            //        DateOfBirth = table.Column<DateTime>(type: "datetime", nullable: false),
            //        Relationships = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
            //        EmpIdFk = table.Column<int>(type: "int", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__Dependen__9BC67CF15ABA19F9", x => x.DependentId);
            //        table.ForeignKey(
            //            name: "FK__Dependent__EmpId__403A8C7D",
            //            column: x => x.EmpIdFk,
            //            principalTable: "Employee",
            //            principalColumn: "EmpId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "WorksOn",
            //    columns: table => new
            //    {
            //        WorksOnId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        WorkingHours = table.Column<int>(type: "int", nullable: false),
            //        EmpIdFk = table.Column<int>(type: "int", nullable: false),
            //        ProjectIdFk = table.Column<int>(type: "int", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__WorksOn__4ADF734BFFE2D260", x => x.WorksOnId);
            //        table.ForeignKey(
            //            name: "FK__WorksOn__EmpIdFk__5165187F",
            //            column: x => x.EmpIdFk,
            //            principalTable: "Employee",
            //            principalColumn: "EmpId");
            //        table.ForeignKey(
            //            name: "FK__WorksOn__Project__52593CB8",
            //            column: x => x.ProjectIdFk,
            //            principalTable: "Project",
            //            principalColumn: "ProjectId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_DepartmentLocations_DepartmentIdFK",
            //    table: "DepartmentLocations",
            //    column: "DepartmentIdFK");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Dependent_EmpIdFk",
            //    table: "Dependent",
            //    column: "EmpIdFk");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Employee_DepartmentId",
            //    table: "Employee",
            //    column: "DepartmentId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Employee_SupervisorId",
            //    table: "Employee",
            //    column: "SupervisorId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Project_DepartmentIdFk",
            //    table: "Project",
            //    column: "DepartmentIdFk");

            //migrationBuilder.CreateIndex(
            //    name: "IX_WorksOn_EmpIdFk",
            //    table: "WorksOn",
            //    column: "EmpIdFk");

            //migrationBuilder.CreateIndex(
            //    name: "IX_WorksOn_ProjectIdFk",
            //    table: "WorksOn",
            //    column: "ProjectIdFk");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            //migrationBuilder.DropTable(
            //    name: "DepartmentLocations");

            //migrationBuilder.DropTable(
            //    name: "Dependent");

            //migrationBuilder.DropTable(
            //    name: "WorksOn");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            //migrationBuilder.DropTable(
            //    name: "Employee");

            //migrationBuilder.DropTable(
            //    name: "Project");

            //migrationBuilder.DropTable(
            //    name: "Department");
        }
    }
}
