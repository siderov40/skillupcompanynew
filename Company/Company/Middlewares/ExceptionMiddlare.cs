﻿using Company.DTOModels;
using Newtonsoft.Json;
using Polly;
using System.Net;

namespace Company.Middlewares
{
    public class ExceptionMiddlare
    {
        private readonly RequestDelegate _next;
        public ExceptionMiddlare(RequestDelegate next) {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch(Exception ex) 
            {
                await HandleException(ex,httpContext);
            }   
        }

        private Task HandleException(Exception ex, HttpContext httpContext)
        {
            ExceptionHandling exceptionHandling = new ExceptionHandling();
            exceptionHandling.message = ex.Message;
            exceptionHandling.status = (int)HttpStatusCode.InternalServerError;
            exceptionHandling.hasError = true;
            exceptionHandling.innerException = ex.InnerException != null ?
                ex.InnerException.Message : "Middleware";

            return httpContext.Response.WriteAsync(JsonConvert.SerializeObject(exceptionHandling));
        }
    }
}
