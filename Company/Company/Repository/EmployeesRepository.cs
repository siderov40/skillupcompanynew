﻿using Company.DTOModels;
using Company.Models;
using Company.Repository.Interface;
using Company.Wrappers;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Company.Repository
{
    public class EmployeesRepository : IEmployeesRepository
    {
        CompanyContext context;
        public EmployeesRepository(CompanyContext companyContex)
        {
            context = companyContex;
        }
        public async Task<string> createEmp(Employee employee)
        {
            context.Employees.Add(employee);
            await context.SaveChangesAsync();
            return "Employe is created";
        }

        public async Task<string> deleteEmp(Employee employee)
        {
            context.Employees.Remove(employee);
            await context.SaveChangesAsync();
            return "Emp with id " + employee.EmpId + " is deleted";
        }

        public async Task<List<Employee>> getAllEmps()
        {
            return context.Employees.ToList();
        }

        public List<Employee> getAllEmpsPages(PagedFilter paged)
        {
            if (!string.IsNullOrEmpty(paged.SortColumn)
                && !string.IsNullOrEmpty(paged.SortDirection))
            {
                if (paged.SortDirection.Equals("asc"))
                {
                    return context.Employees.OrderByMember(paged.SortColumn, false)
                        .Skip((paged.PageNumber - 1) * paged.PageSize)
                        .Take(paged.PageSize).ToList();
                }
                else
                {
                    return context.Employees.OrderByMember(paged.SortColumn,true)
                        .Skip((paged.PageNumber - 1) * paged.PageSize)
                        .Take(paged.PageSize).ToList();
                }
            }
            else
            {
                return context.Employees
                        .Skip((paged.PageNumber - 1) * paged.PageSize)
                        .Take(paged.PageSize).ToList();
            }
        }

        public async Task<Employee> getEmpById(int id)
        {
            Employee employees = context.Employees.AsNoTracking().Where(s => s.EmpId == id).FirstOrDefault();
            if(employees == null)
            {
                throw new Exception("Employee is not found");
            }
            return employees;
        }

        public int returnEmpTotalCount()
        {
            return context.Employees.Count();
        }

        public async Task<string> updateEmp(Employee employee)
        {
            context.Update(employee);
            await context.SaveChangesAsync();
            return "Update is succesfful";
        }
    }

    public static partial class QueryableExtensions
    {
        public static IOrderedQueryable<T> OrderByMember<T>(this IQueryable<T> source, string memberPath, bool descending)
        {
            var parameter = Expression.Parameter(typeof(T), "item");
            var member = memberPath.Split('.')
                .Aggregate((Expression)parameter, Expression.PropertyOrField);
            var keySelector = Expression.Lambda(member, parameter);
            var methodCall = Expression.Call(
                typeof(Queryable), descending ? "OrderByDescending" : "OrderBy",
                new[] { parameter.Type, member.Type },
                source.Expression, Expression.Quote(keySelector));
            return (IOrderedQueryable<T>)source.Provider.CreateQuery(methodCall);
        }
    }
}
