﻿using Company.DTOModels;
using Company.Models;
using Company.Wrappers;

namespace Company.Repository.Interface
{
    public interface IEmployeesRepository
    {
        public Task<List<Employee>> getAllEmps();
        public Task<Employee> getEmpById(int id);
        public int returnEmpTotalCount();
        public Task<string> createEmp(Employee employees);
        public Task<string> updateEmp(Employee employees);
        public Task<string> deleteEmp(Employee employee);
        List<Employee> getAllEmpsPages(PagedFilter paged);
    }
}
