﻿using Company.DTOModels;

namespace Company.Repository.Interface
{
    public interface IWorksOnRepository
    {
        public Task<List<WorksOnDTO>> GetWorkingHours();
    }
}
