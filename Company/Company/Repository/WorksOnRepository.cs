﻿using Company.DTOModels;
using Company.Models;
using Company.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace Company.Repository
{
    public class WorksOnRepository : IWorksOnRepository
    {
        CompanyContext _companyContext;
        public WorksOnRepository(CompanyContext companyContext)
        {
            _companyContext = companyContext;
        }
        public async Task<List<WorksOnDTO>> GetWorkingHours()
        {
            List<WorksOnDTO> worksOnDTOs =
                _companyContext.WorksOns
                    .Include(s => s.ProjectIdFkNavigation)
                    .Include(s => s.EmpIdFkNavigation)
                    .Select(s =>
                        new WorksOnDTO
                        {
                            WorkingHours = s.WorkingHours,
                            WorksOnId = s.WorksOnId,
                            EmpName = s.EmpIdFkNavigation.EmpFirstName,
                            ProjectName = s.ProjectIdFkNavigation.ProjectName
                        }
                    ).ToList();
            return worksOnDTOs;
        }
    }
}
