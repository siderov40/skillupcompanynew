﻿using System.Security.Cryptography;
using System.Text;

namespace Company.Helpers
{
    public class Helpers
    {
        public static string password = "lm0gzloup69q3rioxt1xzps035y4s555";
        public static string Encript(string input)
        {
            System.Security.Cryptography.Aes aes =
            System.Security.Cryptography.Aes.Create();

            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;
            aes.KeySize = 256;
            aes.Key = Encoding.UTF8.GetBytes(password);
            aes.IV = new byte[16];
            var encriptor = aes.CreateEncryptor(aes.Key, aes.IV);

            using (var msEncrypt = new MemoryStream())
            {
                using (var csEncrypt = new CryptoStream(msEncrypt, encriptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter scEncryptWriter = new StreamWriter(csEncrypt))
                    {
                        scEncryptWriter.Write(input);
                    }

                }
                var result = Convert.ToBase64String(msEncrypt.ToArray());
                return result;
            }

        }

        public static string Decrypt(string input)
        {
            string decryptedText = "";

            if (!string.IsNullOrEmpty(input))
            {
                byte[] fullCipher = Convert.FromBase64String(input);

                System.Security.Cryptography.Aes aes =
                System.Security.Cryptography.Aes.Create();

                aes.Padding = PaddingMode.PKCS7;
                aes.Mode = CipherMode.CBC;
                aes.KeySize = 256;
                aes.Key = Encoding.UTF8.GetBytes(password);
                aes.IV = new byte[16];

                var decriptor =
                    aes.CreateDecryptor(aes.Key, aes.IV);
                using (var msEncrypt = new MemoryStream(fullCipher))
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, decriptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader scEncryptWriter = new StreamReader(csEncrypt))
                        {
                            decryptedText = scEncryptWriter.ReadToEnd();
                        }

                    }
                }
            }
            return decryptedText;
        }
    }
}
