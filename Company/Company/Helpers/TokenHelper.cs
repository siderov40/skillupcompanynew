﻿using Company.DTOModels;
using Company.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Company.Helpers
{
    public class TokenHelper
    {
        public static string GenerateToken(User user)
        {
            if(user == null)
            {
                return "";
            }

            Claim[] claims = new Claim[]
            {
                new Claim("UserID", user.Id),
                new Claim("UserName", user.UserName),
                new Claim("FirstName", user.FirstName),
                new Claim("Email", user.Email),
                new Claim("Roles", "Admin")
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("23231VSscS?#23124c?!"));

            var token =
                new JwtSecurityToken(null, null, claims, null, DateTime.Now.AddDays(1),
                new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature));
           

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public static AuthUser readJwt(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            var date = new DateTime(int.Parse(jwtSecurityToken.Claims.First(s => s.Type == "exp").Value));
            if (date.Ticks >= DateTime.UtcNow.Ticks)
            {
                throw new Exception("User is not authenticated");
            }


            var roles = jwtSecurityToken.Claims.First(s => s.Type == "FirstName").Value;
            AuthUser authUser = new AuthUser()
            {
                UserName = jwtSecurityToken.Claims.First(s => s.Type == "UserName").Value,
                Role = jwtSecurityToken.Claims.First(s => s.Type == "Roles").Value,
                FirstName = jwtSecurityToken.Claims.First(s => s.Type == "FirstName").Value
            };
           

            return authUser;
        }
    }
}
