﻿using AutoMapper;
using Company.DTOModels;
using Company.Models;

namespace Company.Mappers
{
    public class EmployeeMapper : Profile
    {
        public EmployeeMapper()
        {
            CreateMap<EmployeeDTO, Employee>();
            CreateMap<EmployeeDTO, Employee>().ReverseMap()
                .ForMember(s => s.EmpId, opt => 
                opt.MapFrom(s => Helpers.Helpers.Encript(s.EmpId.ToString())))
                .ForMember(s => s.DepartmentId, opt => 
                opt.MapFrom(s =>  Helpers.Helpers.Encript(s.DepartmentId.ToString())))
                .ForMember(s => s.SupervisorId, opt => opt.MapFrom(
                    s => Helpers.Helpers.Encript(s.SupervisorId.ToString())));
        }
    }
}
