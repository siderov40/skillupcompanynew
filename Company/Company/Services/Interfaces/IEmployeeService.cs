﻿using Company.DTOModels;
using Company.Wrappers;

namespace Company.Services.Interfaces
{
    public interface IEmployeeService
    {
        Task<List<EmployeeDTO>> getAllEmps();
        Task<EmployeeDTO> getEmpById(string id);
        Task<string> createEmp(EmployeeDTO emp);
        Task<string> updateEmp(EmployeeDTO value);
        Task<string> deleteEmp(string id);
        PagedResponse<List<EmployeeDTO>> getEmpPaged(PagedFilter paged);
    }
}
