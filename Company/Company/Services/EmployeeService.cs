﻿using AutoMapper;
using Company.DTOModels;
using Company.Models;
using Company.Repository.Interface;
using Company.Services.Interfaces;
using Company.Wrappers;

namespace Company.Services
{
    public class EmployeeService : IEmployeeService
    {
        IEmployeesRepository _employeesRepository;
        IMapper _mapper;
        public EmployeeService(IEmployeesRepository employeesRepository,
            IMapper mapper)
        {
            _employeesRepository = employeesRepository;
            _mapper = mapper;
        }

        public async Task<string> createEmp(EmployeeDTO employeeDTO)
        {
            Employee employee = _mapper.Map<Employee>(employeeDTO);

            #region WITHOUT AUTOMAPPER
            //employee.EmpDateOfBirth = employeeDTO.EmpDateOfBirth;
            //employee.EmpFirstName = employeeDTO.EmpFirstName;
            //employee.EmpLastName = employeeDTO.EmpLastName;
            //employee.EmpAddress = employeeDTO.EmpAddress;
            //employee.Salary = employeeDTO.Salary;
            //employee.Gender = employeeDTO.Gender;
            //employee.EmpMiddleName = employeeDTO.EmpMiddleName;
            //employee.DepartmentId = employeeDTO.DepartmentId;
            //employee.SupervisorId = employeeDTO.SupervisorId;
            //employee.Ssn = employeeDTO.Ssn;
            #endregion

            return await _employeesRepository.createEmp(employee);
        }

        public async Task<string> deleteEmp(string id)
        {
            Employee employees = await _employeesRepository
                .getEmpById(int.Parse(Helpers.Helpers.Decrypt(id)));
            if (employees == null)
            {
                return "Emp with id " + id + " is not exits";
            }


            return await _employeesRepository.deleteEmp(employees);
        }

        public async Task<List<EmployeeDTO>> getAllEmps()
        {
            List<Employee> employeesResponse = await _employeesRepository.getAllEmps();

            List<EmployeeDTO> employeeDTOs = _mapper.Map<List<EmployeeDTO>>(employeesResponse);

            #region MAPPING WITHOUT AUTOMAPPER
            //foreach (var employees in employeesResponse)
            //{
            //    EmployeeDTO employeeDTO = new EmployeeDTO();
            //    employeeDTO.EmpFirstName = employees.EmpFirstName;
            //    employeeDTO.EmpLastName = employees.EmpLastName;
            //    employeeDTO.EmpMiddleName = employees.EmpMiddleName;
            //    employeeDTO.Salary = employees.Salary;
            //    employeeDTO.DepartmentId = employees.DepartmentId;
            //    employeeDTO.Gender = employees.Gender;
            //    employeeDTO.EmpAddress = employees.EmpAddress;
            //    employeeDTO.EmpDateOfBirth = employees.EmpDateOfBirth;
            //    employeeDTOs.Add(employeeDTO);
            //}
            #endregion

            return employeeDTOs;
        }

        public async Task<EmployeeDTO> getEmpById(string id)
        {
            Employee employees = await _employeesRepository
                .getEmpById(int.Parse(Helpers.Helpers.Decrypt(id)));

            EmployeeDTO employeeDTO = _mapper.Map<EmployeeDTO>(employees);

            #region WITHOUT AUTOMAPPER
            //employeeDTO = new EmployeeDTO();
            //employeeDTO.EmpFirstName = employees.EmpFirstName;
            //employeeDTO.EmpLastName = employees.EmpLastName;
            //employeeDTO.EmpMiddleName = employees.EmpMiddleName;
            //employeeDTO.Salary = employees.Salary;
            //employeeDTO.DepartmentId = employees.DepartmentId;
            //employeeDTO.Gender = employees.Gender;
            //employeeDTO.EmpAddress = employees.EmpAddress;
            //employeeDTO.EmpDateOfBirth = employees.EmpDateOfBirth;
            #endregion

            return employeeDTO;
        }

        public PagedResponse<List<EmployeeDTO>> getEmpPaged(PagedFilter paged)
        {
            List<Employee> response = _employeesRepository.getAllEmpsPages(paged);
            List<EmployeeDTO> employeeDTOs = _mapper.Map<List<EmployeeDTO>>(response);

            int totalCount = _employeesRepository.returnEmpTotalCount();
            PagedResponse<List<EmployeeDTO>> response_wrapped = new PagedResponse<List<EmployeeDTO>>()
            {
                Data = employeeDTOs,
                TotalCount = totalCount,
                PageNumber = paged.PageNumber,
                TotalPages = (totalCount / paged.PageSize)+1,
                PageSize = paged.PageSize
            };

            return response_wrapped;

        }

        public async Task<string> updateEmp(EmployeeDTO employeeDto)
        {
            Employee employee = await _employeesRepository.getEmpById(int.Parse(Helpers.Helpers.Decrypt(employeeDto.EmpId)));

            if (employee == null)
            {
                throw new Exception("Employee doesnt exit");
            }
            employee = _mapper.Map<Employee>(employeeDto);

            #region WITHOUT AUTOMAPPER 
            //employee.EmpId = employeeDto.EmpId;
            //employee.EmpDateOfBirth = employeeDto.EmpDateOfBirth;
            //employee.EmpFirstName = employeeDto.EmpFirstName;
            //employee.EmpLastName = employeeDto.EmpLastName;
            //employee.EmpAddress = employeeDto.EmpAddress;
            //employee.Salary = employeeDto.Salary;
            //employee.Gender = employeeDto.Gender;
            //employee.EmpMiddleName = employeeDto.EmpMiddleName;
            //employee.DepartmentId = employeeDto.DepartmentId;
            //employee.SupervisorId = employeeDto.SupervisorId;
            //employee.Ssn = employeeDto.Ssn;
            #endregion

            return await _employeesRepository.updateEmp(employee);
        }
    }
}
