﻿namespace Company.DTOModels
{
    public class UserLogin
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
