﻿namespace Company.DTOModels
{
    public class ExceptionHandling
    {
        public string message { get; set; }
        public int status { get; set; }
        public bool hasError { get; set; }
        public string innerException { get; set; }
    }
}
