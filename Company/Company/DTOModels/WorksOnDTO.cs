﻿namespace Company.DTOModels
{
    public class WorksOnDTO
    {
        public int WorksOnId { get; set; }
        public int WorkingHours { get; set; }
        public string EmpName { get; set; }
        public string ProjectName { get; set; }
    }

    public class WorksOnEmployeTotalHoursDTO : WorksOnDTO
    {
        public int TotalHours { get; set; }
    }
}
