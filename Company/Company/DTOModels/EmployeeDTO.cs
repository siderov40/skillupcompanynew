﻿namespace Company.DTOModels
{
    public class EmployeeDTO
    {
        public string EmpId { get; set; }

        public string Ssn { get; set; } = null!;

        public string Gender { get; set; } = null!;

        public string EmpAddress { get; set; } = null!;

        public string EmpFirstName { get; set; } = null!;

        public string? EmpMiddleName { get; set; }

        public string EmpLastName { get; set; } = null!;

        public int Salary { get; set; }

        public string? SupervisorId { get; set; }

        public string? DepartmentId { get; set; }

        public DateTime EmpDateOfBirth { get; set; }

    }
}
