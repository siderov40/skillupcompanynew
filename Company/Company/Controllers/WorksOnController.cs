﻿using Company.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Company.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorksOnController : ControllerBase
    {
        IWorksOnRepository _worksOnRepository;
        ILogger<WorksOnController> _logger; 
        public WorksOnController(IWorksOnRepository worksOnRepository, ILogger<WorksOnController> logger)
        {
            _worksOnRepository = worksOnRepository;
            _logger = logger;
        }

        [HttpGet("GetWorkingHours")]
        public async Task<IActionResult> getWorkingHours()
        {
            try
            {
                _logger.LogInformation("Get working hours");
   
                var response = await _worksOnRepository.GetWorkingHours();
                return Ok(response);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex);
            }
        }
    }
}
