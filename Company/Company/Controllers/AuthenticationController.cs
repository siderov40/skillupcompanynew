﻿using Company.DTOModels;
using Company.Helpers;
using Company.Models;
using Company.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Reflection.Metadata.Ecma335;

namespace Company.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        public AuthenticationController(UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet("GetAllUsers")]
        public async Task<IActionResult> getAllUsers()
        {
            try
            {
                var response = _userManager.Users.ToList();
                List<UserDTO> userDTOs = new List<UserDTO>();
                foreach (var element in response)
                {
                    UserDTO userDTO = new UserDTO();
                    userDTO.UserId = element.Id;
                    userDTO.PhoneNumber = element.PhoneNumber;
                    userDTO.UserName = element.UserName;
                    userDTO.FirstName = element.FirstName;
                    userDTO.LastName = element.LastName;
                    userDTOs.Add(userDTO);
                }

                return Ok(userDTOs);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("CreateUser")]
        public async Task<IActionResult> createUser([FromBody] UserDTO userDto)
        {
            try
            {
                User user = new User()
                {
                    UserName = userDto.UserName,
                    Email = userDto.Email,
                    FirstName = userDto.FirstName,
                    LastName = userDto.LastName,
                    PhoneNumber = userDto.PhoneNumber
                };
                var result = await _userManager.CreateAsync(user,userDto.Password);

                if (result.Succeeded)
                {
                    return Ok("User is created");
                }
                return Ok("User is not created");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("AuthorizeUser")]
        public async Task<IActionResult> AuthUser([FromBody] string token)
        {
            return Ok(TokenHelper.readJwt(token));
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserLogin userLogin)
        {
            try
            {
                User user = _userManager.FindByNameAsync(userLogin.userName).Result;
                if(user != null)
                {
                    var loggedUser = await _signInManager.PasswordSignInAsync(user, userLogin.password, true, true);
                    if (loggedUser.Succeeded)
                    {
                        var token = TokenHelper.GenerateToken(user);
                        Response<string> httpResponse = new Response<string>()
                        {
                            Data = token,
                            Success = true,
                            Error = "",
                            Message = "User logged"
                       
                        };
                        return Ok(httpResponse);
                    }
                    return Ok("Wrong credentials");
                }

                return Ok("User not found");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
