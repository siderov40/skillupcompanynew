﻿using AutoMapper;
using Company.DTOModels;
using Company.Models;
using Company.Repository;
using Company.Repository.Interface;
using Company.Services.Interfaces;
using Company.Wrappers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Company.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        IEmployeeService _employeeService;
        ILogger<EmployeesController> _logger;
        public EmployeesController(IEmployeeService employeeService, ILogger<EmployeesController> logger)
        {
            _employeeService = employeeService;
            _logger = logger;
        }

        // GET: api/<EmployeesController>
        [HttpGet("getAllEmployees")]
        public async Task<IActionResult> GetAllEmployees()
        {
              //  throw new Exception("Exception");
                _logger.Log(LogLevel.Information, "Get all employees");

                List<EmployeeDTO> employeeDTOs = await _employeeService.getAllEmps();

                Wrappers.Response<List<EmployeeDTO>> response =
                new Wrappers.Response<List<EmployeeDTO>>()
                {
                    Data = employeeDTOs,
                    Success = true,
                    Error = null,
                    Message = ""
                };

                return Ok(response);
           // }
            //catch(Exception ex)
            //{
            //    _logger?.Log(LogLevel.Error, ex.Message);
            //    return BadRequest(ex.Message);
            //}
        }

        // GET api/<EmployeesController>/5
        [HttpGet("getEmpBy/{id}")]
        public async Task<IActionResult> GetEmployeeById(string id)
        {
            try
            {
                EmployeeDTO employeeDTO = await _employeeService.getEmpById(id);
                return Ok(employeeDTO);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<EmployeesController>
        [HttpPost("createEmployee")]
        public async Task<IActionResult> Post([FromBody] EmployeeDTO emp)
        {
            try
            {
                string response = await _employeeService.createEmp(emp);
                return Ok(response);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<EmployeesController>/5
        [HttpPost("updateEmployee")]
        public async Task<IActionResult> updateEmployee([FromBody] EmployeeDTO value)
        {
            try
            {
                string response = await _employeeService.updateEmp(value);
                return Ok(response);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<EmployeesController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                string response = await _employeeService.deleteEmp(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("getEmpPaged")]
        public async Task<IActionResult> getEmployeesPaged(PagedFilter paged)
        {
            PagedResponse<List<EmployeeDTO>> reponse = _employeeService.getEmpPaged(paged);

            return Ok(reponse); 
        }
    }
}
