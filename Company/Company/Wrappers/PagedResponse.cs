﻿namespace Company.Wrappers
{
    public class PagedResponse<T> : Response<T>
    {
        public int PageNumber { get; set; }
        public int TotalCount { get; set; } = 0;
        public int PageSize { get; set; }
        public int TotalPages { get; set; }

        public PagedResponse()
        {

        }

        public PagedResponse(T Data, int PageNumber, int TotalCount, int PageSize, int TotalPages)
            :base(Data, true, null, "")
        {
            this.PageNumber= PageNumber;
            this.TotalCount= TotalCount;
            this.PageSize= PageSize;
            this.TotalPages= TotalPages;
        }
    }
}
