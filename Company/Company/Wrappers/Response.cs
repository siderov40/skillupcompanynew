﻿namespace Company.Wrappers
{
    public class Response<T>
    {
        public T Data { get; set; }
        public bool Success { get; set; }
        public string Error { get; set; }
        public string Message { get; set; }

        public Response()
        {

        }
        public Response(T data, bool success, string error, string message)
        {
            Data = data;
            Success = success;
            Error = error;
            Message = message;
        }
    }
}
