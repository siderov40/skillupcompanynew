﻿namespace Company.Wrappers
{
    public class PagedFilter
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }

        public PagedFilter()
        {

        }

    }
}
