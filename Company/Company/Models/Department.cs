﻿using System;
using System.Collections.Generic;

namespace Company.Models;

public partial class Department
{
    public int DepartmentId { get; set; }

    public string DepartmentName { get; set; } = null!;

    public int DepartmentNumber { get; set; }

    public DateTime? ManagerStartDate { get; set; }

    public virtual ICollection<DepartmentLocation> DepartmentLocations { get; } = new List<DepartmentLocation>();

    public virtual ICollection<Employee> Employees { get; } = new List<Employee>();

    public virtual ICollection<Project> Projects { get; } = new List<Project>();
}
