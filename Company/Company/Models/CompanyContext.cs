﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Company.Models;

public partial class CompanyContext: IdentityDbContext<User>
{
    public CompanyContext()
    {
    }

    public CompanyContext(DbContextOptions<CompanyContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Department> Departments { get; set; }

    public virtual DbSet<DepartmentLocation> DepartmentLocations { get; set; }

    public virtual DbSet<Dependent> Dependents { get; set; }

    public virtual DbSet<Employee> Employees { get; set; }

    public virtual DbSet<Project> Projects { get; set; }

    public virtual DbSet<WorksOn> WorksOns { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=WINDOWS-MUNF8E1;Database=CompanyLast;Integrated Security=true;Trust Server Certificate=true");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Department>(entity =>
        {
            entity.HasKey(e => e.DepartmentId).HasName("PK__Departme__B2079BEDD5972032");

            entity.ToTable("Department");

            entity.Property(e => e.DepartmentName).IsUnicode(false);
            entity.Property(e => e.ManagerStartDate).HasColumnType("datetime");
        });

        modelBuilder.Entity<DepartmentLocation>(entity =>
        {
            entity.HasKey(e => e.DepartmentLocationId).HasName("PK__Departme__B6760B13BF8AADBB");

            entity.Property(e => e.DepartmentIdFk).HasColumnName("DepartmentIdFK");
            entity.Property(e => e.DepartmentLocationName).IsUnicode(false);

            entity.HasOne(d => d.DepartmentIdFkNavigation).WithMany(p => p.DepartmentLocations)
                .HasForeignKey(d => d.DepartmentIdFk)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__Departmen__Depar__398D8EEE");
        });

        modelBuilder.Entity<Dependent>(entity =>
        {
            entity.HasKey(e => e.DependentId).HasName("PK__Dependen__9BC67CF15ABA19F9");

            entity.ToTable("Dependent");

            entity.Property(e => e.DateOfBirth).HasColumnType("datetime");
            entity.Property(e => e.DependentName).IsUnicode(false);
            entity.Property(e => e.Gender)
                .HasMaxLength(1)
                .IsUnicode(false);
            entity.Property(e => e.Relationships).IsUnicode(false);

            entity.HasOne(d => d.EmpIdFkNavigation).WithMany(p => p.Dependents)
                .HasForeignKey(d => d.EmpIdFk)
                .HasConstraintName("FK__Dependent__EmpId__403A8C7D");
        });

        modelBuilder.Entity<Employee>(entity =>
        {
            entity.HasKey(e => e.EmpId).HasName("PK__Employee__AF2DBB99E15CC0EC");

            entity.ToTable("Employee");

            entity.Property(e => e.EmpAddress).IsUnicode(false);
            entity.Property(e => e.EmpDateOfBirth).HasColumnType("datetime");
            entity.Property(e => e.EmpFirstName).IsUnicode(false);
            entity.Property(e => e.EmpLastName).IsUnicode(false);
            entity.Property(e => e.EmpMiddleName).IsUnicode(false);
            entity.Property(e => e.Gender)
                .HasMaxLength(1)
                .IsUnicode(false);
            entity.Property(e => e.Ssn)
                .HasMaxLength(14)
                .IsUnicode(false);

            entity.HasOne(d => d.Department).WithMany(p => p.Employees)
                .HasForeignKey(d => d.DepartmentId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__Employee__Depart__3D5E1FD2");

            entity.HasOne(d => d.Supervisor).WithMany(p => p.InverseSupervisor)
                .HasForeignKey(d => d.SupervisorId)
                .HasConstraintName("FK__Employee__Superv__3C69FB99");
        });

        modelBuilder.Entity<Project>(entity =>
        {
            entity.HasKey(e => e.ProjectId).HasName("PK__Project__761ABEF014C29242");

            entity.ToTable("Project");

            entity.Property(e => e.ProjectLocation).IsUnicode(false);
            entity.Property(e => e.ProjectName).IsUnicode(false);

            entity.HasOne(d => d.DepartmentIdFkNavigation).WithMany(p => p.Projects)
                .HasForeignKey(d => d.DepartmentIdFk)
                .HasConstraintName("FK__Project__Departm__4316F928");
        });

        modelBuilder.Entity<WorksOn>(entity =>
        {
            entity.HasKey(e => e.WorksOnId).HasName("PK__WorksOn__4ADF734BFFE2D260");

            entity.ToTable("WorksOn");

            entity.HasOne(d => d.EmpIdFkNavigation).WithMany(p => p.WorksOns)
                .HasForeignKey(d => d.EmpIdFk)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__WorksOn__EmpIdFk__5165187F");

            entity.HasOne(d => d.ProjectIdFkNavigation).WithMany(p => p.WorksOns)
                .HasForeignKey(d => d.ProjectIdFk)
                .HasConstraintName("FK__WorksOn__Project__52593CB8");
        });

        OnModelCreatingPartial(modelBuilder);
        base.OnModelCreating(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

}