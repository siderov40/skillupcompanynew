﻿using System;
using System.Collections.Generic;

namespace Company.Models;

public partial class Employee
{
    public int EmpId { get; set; }

    public string Ssn { get; set; } = null!;

    public string Gender { get; set; } = null!;

    public string EmpAddress { get; set; } = null!;

    public string EmpFirstName { get; set; } = null!;

    public string? EmpMiddleName { get; set; }

    public string EmpLastName { get; set; } = null!;

    public int Salary { get; set; }

    public int? SupervisorId { get; set; }

    public int? DepartmentId { get; set; }

    public DateTime EmpDateOfBirth { get; set; }

    public virtual Department? Department { get; set; }

    public virtual ICollection<Dependent> Dependents { get; } = new List<Dependent>();

    public virtual ICollection<Employee> InverseSupervisor { get; } = new List<Employee>();

    public virtual Employee? Supervisor { get; set; }

    public virtual ICollection<WorksOn> WorksOns { get; } = new List<WorksOn>();

}
