﻿using System;
using System.Collections.Generic;

namespace Company.Models;

public partial class Project
{
    public int ProjectId { get; set; }

    public string ProjectName { get; set; } = null!;

    public int ProjectNumber { get; set; }

    public int DepartmentIdFk { get; set; }

    public string ProjectLocation { get; set; } = null!;

    public virtual Department DepartmentIdFkNavigation { get; set; } = null!;

    public virtual ICollection<WorksOn> WorksOns { get; } = new List<WorksOn>();
}
