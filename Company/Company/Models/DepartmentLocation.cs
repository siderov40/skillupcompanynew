﻿using System;
using System.Collections.Generic;

namespace Company.Models;

public partial class DepartmentLocation
{
    public int DepartmentLocationId { get; set; }

    public string DepartmentLocationName { get; set; } = null!;

    public int? DepartmentIdFk { get; set; }

    public virtual Department? DepartmentIdFkNavigation { get; set; }
}
