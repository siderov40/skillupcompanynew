﻿using System;
using System.Collections.Generic;

namespace Company.Models;

public partial class Dependent
{
    public int DependentId { get; set; }

    public string DependentName { get; set; } = null!;

    public string Gender { get; set; } = null!;

    public DateTime DateOfBirth { get; set; }

    public string Relationships { get; set; } = null!;

    public int EmpIdFk { get; set; }

    public virtual Employee EmpIdFkNavigation { get; set; } = null!;
}
