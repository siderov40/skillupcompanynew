﻿using System;
using System.Collections.Generic;

namespace Company.Models;

public partial class WorksOn
{
    public int WorksOnId { get; set; }

    public int WorkingHours { get; set; }

    public int EmpIdFk { get; set; }

    public int ProjectIdFk { get; set; }

    public virtual Employee EmpIdFkNavigation { get; set; } = null!;
    
    public virtual Project ProjectIdFkNavigation { get; set; } = null!;
}
