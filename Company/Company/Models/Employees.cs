﻿namespace Company.Models
{
    public class Employees
    {
        public int EmpId { get; set; }
        public string Snn { get; set; }
        public string EmpAddress { get; set; }
        public string Gender { get; set; }
        public string EmpFirstName { get; set; }
        public string EmpLastName { get; set; }
        public string? EmpMiddleName { get; set; }
        public DateTime EmpDateOfBirth { get; set; }
        public int? SupervisorId { get; set; }
        public int? DepartmentId { get; set; }
        public int Salary { get; set; }
    }
}
