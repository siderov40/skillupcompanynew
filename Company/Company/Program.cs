using Company.DTOModels;
using Company.Filters;
using Company.Middlewares;
using Company.Models;
using Company.Repository;
using Company.Repository.Interface;
using Company.Services;
using Company.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System.Net;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((ctx, lc) => lc
    .WriteTo.File($"logs/log-{DateTime.Now.ToString("ddMMyyyy")}.log"));

// Add services to the container.

builder.Services.AddControllers(config =>
{
    config.Filters.Add(typeof(CustomException));
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IEmployeesRepository, EmployeesRepository>();
builder.Services.AddScoped<IWorksOnRepository, WorksOnRepository>();
builder.Services.AddScoped<IEmployeeService, EmployeeService>();
builder.Services.AddSwaggerGen(opt =>
{
    opt.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme()
    {
        Description = "Authorization with JWT",
        Type = Microsoft.OpenApi.Models.SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        Name = "Company"
    });
});
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(opt =>
    {
        opt.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidateAudience= true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = "skillup.com",
            ValidAudience = "skillup.com",
            IssuerSigningKey = 
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes("23231VSscS?#23124c?!")),

        };
    });
builder.Services.AddDbContextFactory<CompanyContext>(s =>
{
    s.EnableSensitiveDataLogging();
});

#region IDENTITY
builder.Services.AddIdentity<User, IdentityRole>(opt => opt.SignIn.RequireConfirmedAccount = false)
    .AddEntityFrameworkStores<CompanyContext>();
#endregion

#region AUTOMAPPER
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
#endregion



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseHttpsRedirection();

app.UseAuthorization();

app.UseExceptionHandler(s =>
{
    s.Run(async context =>
    {
        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        context.Response.ContentType = "application/json";
        var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
        if (contextFeature != null)
        {
            await context.Response.WriteAsync(new ExceptionHandling()
            {
                status = context.Response.StatusCode,
                message = "Internal Server Error."
            }.ToString());
        }
    });
});

app.MapControllers();

app.UseMiddleware<ExceptionMiddlare>();

app.Run();
